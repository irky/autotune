clear all;
close all;
[x, Fs] = wavread('kasia.wav');

N = length(x) % liczba probek
A = 5; % amplituda
fp = Fs % czestotliwosc probkowania
dt = 1/Fs; % okres probkowania
t = dt*(0:N-1); % wektor czasu
gama = gama();



for i = 1: 100
f(i) = (gama(end) - gama(1))*rand() + gama(1);

f1 = f(i);
x = A*sin(2*pi*f1*t) + 0.5*A*sin(2*pi*1.05*f1*t) +  0.5*A*sin(2*pi*1.03*f1*t) ; % sinusy o roznych czestotliwosciach

hop = 64;
winSize = 256;

% Rotate if needed
if size(x,1) < size(x,2)
   
    x = transpose(x);

end

% Hanning window for overlap-add
wn = hann(winSize*2+1);
%wn = chebwin(winSize*2+1);
wn = wn(2:2:end);
x = [zeros(hop*3,1) ; x];

[y,numberFramesInput] = createFrames(x,hop,winSize);

numberFramesOutput = numberFramesInput;
outputy = zeros(numberFramesOutput,winSize);


dominantFreqs(1:numberFramesInput) = 0;

index = 1;
currentFrame = y(index,:);

currentFrameWindowed = currentFrame .* wn' / sqrt(((winSize/hop)/2));
currentFrameWindowedFFT = fft(currentFrameWindowed);


blad_bez_aprox(i) = approxMaxFreqSimple(currentFrameWindowedFFT, winSize, Fs, f1, 0);
blad_z_aprox(i) = approxMaxFreqSimple(currentFrameWindowedFFT, winSize, Fs, f1, 1);
end 

f = f';
blad_bez_aprox = blad_bez_aprox';
blad_z_aprox = blad_z_aprox';