function [out] = shannon_filter(in)

dolny_prog =0.01;
gorny_prog = 0.90;

if  dolny_prog > 0 && gorny_prog < 1
    [z,p,k] = cheby2(6,20, [dolny_prog, gorny_prog],'bandpass');
    SOS = zp2sos(z,p,k);
    out = sosfilt(SOS,in);
end
end