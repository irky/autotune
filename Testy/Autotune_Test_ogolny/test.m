clear all;
close all;
[x, Fs] = wavread('kasia.wav');
	
N = length(x); % liczba probek
dt = 1/Fs; % okres probkowania
df = 1/(N*dt); %czestotliwosc podstawowa f- = df = 1/T
f_axis = df * (0:N-1); % kolejne czestotliwosci

% Obetnijmy sobie wykresy wynikowe, zeby bylo lepiej widac skale
MAX_COUNT = N/8;

x = shannon_filter(x);
x_fft = abs(fft(x));
figure; stem(f_axis(1:MAX_COUNT), x_fft(1:MAX_COUNT)); 
grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('Sygnal poczatkowy');

[y, newF, oldF] = pitchShift(x, 1024 ,256, Fs);
y_fft = abs(fft(y));
figure;stem(f_axis(1:MAX_COUNT), y_fft(1:MAX_COUNT)); 
grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('Po autotune');

disp('Dominujace czestotliwosci - powinny byc zblizone do tych podanych jako Gama')
for i = 1: MAX_COUNT
    if y_fft(i) > max(y_fft)/2
        f_axis(i)
    end
end

y_fil = shannon_filter(y);

wavwrite(y, Fs, 'kasia_jeden.wav');
wavwrite(y_fil, Fs, 'kasia_jeden_filtr.wav');