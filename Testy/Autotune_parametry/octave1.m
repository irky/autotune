function [octave4] = octave1()
% ==================================================================
% dzwieki z pierwszej oktawy (czestotliwosc w Hz)
% dzwiek o oktawe wyzej/nizej polega na pomnozeniu/podzieleniu danej
% czestotliwosci przez 2
% najlepiej to pozniej zostawic tylko w vectorze
C1 = 262;
Cis1 = 277;
D1 = 294;
Dis1 = 311;
E1 = 330;
F1 = 349;
Fis1 = 370;
G1 = 392;
Gis1 = 415;
A1 = 440;
B1 = 466;
H1 = 494;
C2 = 523;

octave4 = [C1, Cis1, D1, Dis1, E1, F1, Fis1, G1, Gis1, A1, B1, H1, C2];

end
