function[max_freq, max_spectrum, max_spectrum_ID] = interpolation(FFT_window, Fs, with_approximation)

Gama = gama();

window_size = length(FFT_window);
dt = 1/Fs; % okres probkowania
df = 1/(length(FFT_window)*dt); %czestotliwosc podstawowa f- = df = 1/T
f_axis = df * (0:length(FFT_window)-1); % kolejne czestotliwosci

significantFFT = abs(FFT_window);
[~, index] = max(significantFFT);
max_spectrum_ID = index;
max_spectrum = f_axis(index);
freq_corrected = max_spectrum;

step = 0;

if with_approximation > 0
if max_spectrum > Gama(1) && max_spectrum < Gama(end)
    left = 0;
    right = 0;
    
    % druga najwieksza sasiadujaca z max
    if index > 1
        left = significantFFT(index - 1);
    end
    
    if index < window_size
        right = significantFFT(index + 1);
    end
    
    if left > right
        secondFreq = f_axis(index - 1);
    else
        secondFreq = f_axis(index + 1);
    end
    
    % "Metody interpolacji widma ... " ksiazka wzor 2.52 i 2.53
    if secondFreq > 0
        alfa = abs(secondFreq)/abs(max_spectrum);
        delta = (2*alfa - 1)/(alfa +1);
        freq_corrected = (1-delta)*max_spectrum + delta*secondFreq;
    end  
end
end

max_freq = freq_corrected;

end