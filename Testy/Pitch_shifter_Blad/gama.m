function [Gama] = gama()

Oktawa4 = octave1();
Gama = [Oktawa4./4 Oktawa4./2 Oktawa4 Oktawa4.*2 Oktawa4.*4];

end