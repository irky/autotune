% Test przesuwania widma
function [freq_end, f_wanted] = pitchShiftSimpleTest(windowSize, f_start, f_wanted, scale_divider)
% f_start - czestotliwosc jaka ma miec sygnal wejsciowy
% f_wanted - czestotliwosc do ktorej sygnal powinien podazyc
% scale_divider - pomniejsz os X na wykresie tyle razy (przykladowo 128 dla
% f bliskich 100)
% freq_end - taka czestotliwosc osiagnal sygnal

%windowSize = 1024;
hopSize = 256;

[x, Fs] = wavread('kasia.wav'); % Daje sensowna liczbe probek N

N = length(x); % liczba probek
A = 5; % amplituda
fp = Fs; % czestotliwosc probkowania
dt = 1/Fs; % okres probkowania
t = dt*(0:N-1); % wektor czasu
f1 = f_start;
x = A*sin(2*pi*f1*t);
df = 1/(N*dt); %czestotliwosc podstawowa f- = df = 1/T
f_axis = df * (0:N-1); % kolejne czestotliwosci

MAX_COUNT = N/scale_divider;
% 
% x_fft = abs(fft(x));
% figure; stem(f_axis(1:MAX_COUNT), x_fft(1:MAX_COUNT),'x',  'LineWidth',2);
% grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('Pitch shift');


%% Parameters
% Window size
winSize = windowSize;
% Space between windows
hop = hopSize;
% Hanning window for overlap-add
wn = hann(winSize*2+1);
%wn = chebwin(winSize*2+1);
wn = wn(2:2:end);

%% Source file

% Rotate if needed
if size(x,1) < size(x,2)
    x = transpose(x);
end

x = [zeros(hop*3,1) ; x];

%% Initialization

% Create a frame matrix for the current input
[y,numberFramesInput] = createFrames(x,hop,winSize);

% Create a frame matrix to receive processed frames
numberFramesOutput = numberFramesInput;
outputy = zeros(numberFramesOutput,winSize);

% Initialize cumulative phase
phaseCumulative = 0;

% Initialize previous frame phase
previousPhase = 0;

dominantFreqs(1:numberFramesInput) = 0;

for index=1:numberFramesInput
    
    %% Analysis
    
    % Get current frame to be processed
    currentFrame = y(index,:);
    
    % Window the frame
    currentFrameWindowed = currentFrame .* wn' / sqrt(((winSize/hop)/2));
    currentFrameWindowedFFT = fft(currentFrameWindowed);
    
    if f_start > f_wanted
        % dodatni step
        step = ((f_wanted - f_start)) / (f_wanted*1.059463 - f_wanted);
    else
        % ujemny step
        step = ((f_wanted - f_start)) / (f_wanted - f_wanted/1.059463);
    end
    
    
    % Pitch scaling factor
    alpha = 2^(step/12);
    
    % Intermediate constants
    hopOut = round(alpha*hop);
    
    % Get the magnitude
    magFrame = abs(currentFrameWindowedFFT);
    
    % Get the angle
    phaseFrame = angle(currentFrameWindowedFFT);
    
    %% Processing
    
    % Get the phase difference
    deltaPhi = phaseFrame - previousPhase;
    previousPhase = phaseFrame;
    
    % Remove the expected phase difference
    deltaPhiPrime = deltaPhi - hop * 2*pi*(0:(winSize-1))/winSize;
    
    % Map to -pi/pi range
    deltaPhiPrimeMod = mod(deltaPhiPrime+pi, 2*pi) - pi;
    
    % Get the true frequency
    trueFreq = 2*pi*(0:(winSize-1))/winSize + deltaPhiPrimeMod/hop;
    
    % Get the final phase
    phaseCumulative = phaseCumulative + hopOut * trueFreq;
    
    % Remove the 60 Hz noise. This is not done for now but could be
    % achieved by setting some bins to zero.
    
    %% Synthesis
    
    % Get the magnitude
    outputMag = magFrame;
    
    % Produce output frame
    outputFrame = real(ifft(outputMag .* exp(1i*phaseCumulative)));
    
    % Save frame that has been processed
    outputy(index,:) = outputFrame .* wn' / sqrt(((winSize/hopOut)/2));

end


%% Finalize

% Overlap add in a vector
outputTimeStretched = fusionFrames(outputy,hopOut);

% Resample with linearinterpolation
outputTime = interp1((0:(length(outputTimeStretched)-1)),outputTimeStretched,(0:alpha:(length(outputTimeStretched)-1)),'linear');

% Return the result
outputVector = outputTime;

outputVector_fft = abs(fft(outputVector));
%figure; 
% hold on;
% stem(f_axis(1:MAX_COUNT), outputVector_fft(1:MAX_COUNT), 'b*',  'LineWidth',2);
% 
% plot(f_wanted, 0,'o', 'LineWidth',2,...
%                 'MarkerEdgeColor','k',...
%                 'MarkerFaceColor',[.49 1 .63],...
%                 'MarkerSize',12);
%             
% plot(f_start, 0,'s', 'LineWidth',2,...
%                 'MarkerEdgeColor','k',...
%                 'MarkerFaceColor','g',...
%                 'MarkerSize',12);
%grid; grid minor; title('Abs(outputVector)'); xlabel('Hz'); title('Sygnal koncowy');

[~, index] = max(outputVector_fft);
[freq_end, max_spectrum, max_spectrum_ID] = interpolation(outputVector_fft, Fs, 1);

return