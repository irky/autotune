function [error] = approxMaxFreqSimple(FFT_window, window_size, Fs, f1, improvedVersion)

% Zwraca blad popelniony przy wyznaczaniu dominujacej skladowej
% FFT_window - okno po transformacie fouriera
% window_size - rozmiar okna
% Fs - czestotliwosc probkowania utworu
% f1 - prawdziwa dominujaca czestotoliwosc
% improvedVersion - 1 jesli stosowac poprawiona aproksymacje, 0 jesli nie
% stosowac

Gama = gama();

dt = 1/Fs; % okres probkowania
df = 1/(length(FFT_window)*dt); %czestotliwosc podstawowa f- = df = 1/T
f_axis = df * (0:length(FFT_window)-1); % kolejne czestotliwosci

significantFFT = abs(FFT_window);
[~, index] = max(significantFFT);
freq = f_axis(index);

if improvedVersion > 0
    if freq > Gama(1) && freq < Gama(end)
        left = 0;
        right = 0;
        
        % druga najwieksza sasiadujaca z max
        if index > 1
            left = significantFFT(index - 1);
        end
        
        if index < window_size
            right = significantFFT(index + 1);
        end
        
        if left > right
            secondFreq = f_axis(index - 1);
        else
            secondFreq = f_axis(index + 1);
        end
        
        % "Metody interpolacji widma ... " ksiazka wzor 2.52 i 2.53
        if secondFreq > 0
            alfa = abs(secondFreq)/abs(freq);
            delta = (2*alfa - 1)/(alfa +1);
            freq_corrected = delta*freq + (1-delta)*secondFreq;
            freq = freq_corrected;
        end
    end
else
    
end

error = abs(freq - f1);
end
