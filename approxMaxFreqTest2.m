function [freq, wantedFreqOut, step, ID] = approxMaxFreqTest2(FFT_window, window_size, Fs, with_approximation)
% Zwraca dominujaca czestotliwosc
% przyblizana metoda aproksymacji
% FFT_window - okno po transformacie fouriera
% Fs - czestotliwosc probkowania utworu
% with_approximation - czy stosowac aproksymacje Borka

Gama = gama();

[freq, spectrum, ID] = interpolation(FFT_window, Fs, with_approximation);


% szukam tonu najblizszego dominujacej czestotliwosci w Gamie dzwiekow
for j = 1:length(Gama)
    diff(j) = abs(Gama(j) - freq);
end

[minDiff, index] = min(diff);
newFreq = Gama(index);

% magiczna liczba 1.059463 oznacza przesuniecie o polton
if freq > newFreq
    % dodatni step
    step = ((Gama(index) - freq)) / (Gama(index)*1.059463 - Gama(index));
else
    % ujemny step
    step = ((Gama(index) - freq)) / (Gama(index) - Gama(index)/1.059463);
end

wantedFreqOut = newFreq;

end
