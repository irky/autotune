clear all;
gama = gama();

% dla kazdego przedzialu w gamie wylosuj dzwiek z tego przedzialu
% i przesun go do najblizszej nuty
% wykres zale�no�ci b��du 

for i = 1: length(gama)-1
    f_start_v(i) = (gama(i+1) - gama(i))*rand() + gama(i);
    f_start = f_start_v(i);
    diff_left = abs(f_start - gama(i));
    diff_right = abs(f_start - gama(i+1));
    
    f_wanted = gama(i+1);
    if (diff_left < diff_right)
        f_wanted = gama(i);
    end;
    f_wanted_v(i) = f_wanted;
    
    [f_end, f_wanted] = pitchShiftSimpleTest(f_start, f_wanted, 8);
    f_end_v(i) = f_end;
    
end

f_start_v = f_start_v';
f_end_v = f_end_v';
f_wanted_v = f_wanted_v';