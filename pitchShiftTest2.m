
function [outputVector, dominantFreqs, realDominantFreqs] = pitchShiftTest2(inputVector, windowSize, hopSize, Fs, with_approx, multiply)

%% Parameters

% Window size
winSize = windowSize;
% Space between windows
hop = hopSize;

% Hanning window for overlap-add
wn = hann(winSize*2+1);
%wn = chebwin(winSize*2+1);
wn = wn(2:2:end);

%% Source file

x = inputVector;

% Rotate if needed
if size(x,1) < size(x,2)
   
    x = transpose(x);

end

x = [zeros(hop*3,1) ; x];

%% Initialization

% Create a frame matrix for the current input
[y,numberFramesInput] = createFrames(x,hop,winSize);

% Create a frame matrix to receive processed frames
numberFramesOutput = numberFramesInput;
outputy = zeros(numberFramesOutput,winSize);

% Initialize cumulative phase
phaseCumulative = 0;

% Initialize previous frame phase
previousPhase = 0;

dominantFreqs(1:numberFramesInput) = 0;

for index=1:numberFramesInput
    
%% Analysis
    
    % Get current frame to be processed
    currentFrame = y(index,:);
    
    % Window the frame
    currentFrameWindowed = currentFrame .* wn' / sqrt(((winSize/hop)/2));
    currentFrameWindowedFFT = fft(currentFrameWindowed);
    
    Gama = gama();

    % Find dominant frequency in given window
    [oldDominantFreq, dominantFreq, step, ID] = approxMaxFreqTest2(currentFrameWindowedFFT, winSize, Fs, with_approx);

  
    scale_divider = 16;
    MAX_COUNT = windowSize/scale_divider;
    
    from = 10;
    
%     if index == 130
%         dt = 1/Fs; % okres probkowania
%         df = 1/(length(currentFrameWindowedFFT)*dt); %czestotliwosc podstawowa f- = df = 1/T
%         f_axis = df * (0:length(currentFrameWindowedFFT)-1); % kolejne czestotliwosci
%         x_fft = abs(currentFrameWindowedFFT);
%         figure; hold on; stem(f_axis(from:MAX_COUNT), x_fft(from:MAX_COUNT),'x',  'LineWidth',2);
%         grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('Before');
%         
% %         plot(oldDominantFreq, 0,'s', 'LineWidth',2,...
% %             'MarkerEdgeColor','k',...
% %             'MarkerFaceColor','r',...
% %             'MarkerSize',10);
%         
% 
%         
% %         plot(oldDominantFreq_with, 0,'s', 'LineWidth',2,...
% %             'MarkerEdgeColor','k',...
% %             'MarkerFaceColor','b',...
% %             'MarkerSize',10);
% %         
% %         plot(dominantFreq_with, 0,'o', 'LineWidth',2,...
% %             'MarkerEdgeColor','k',...
% %             'MarkerFaceColor','b',...
% %             'MarkerSize',10);
%         
%     end
    
    oldDominantFreqs(index) = oldDominantFreq;
    dominantFreqs(index) = dominantFreq; % tu freq pozadanej nuty
     

%    
    
    % Pitch scaling factor
    alpha = 2^(step/12);

    % Intermediate constants
    hopOut = round(alpha*hop);
    
    % Get the magnitude
    magFrame = abs(currentFrameWindowedFFT);
    
    magFrame(ID) = magFrame(ID)*multiply;
    
    % Get the angle
    phaseFrame = angle(currentFrameWindowedFFT);
    
%% Processing    

    % Get the phase difference
    deltaPhi = phaseFrame - previousPhase;
    previousPhase = phaseFrame;
    
    % Remove the expected phase difference
    deltaPhiPrime = deltaPhi - hop * 2*pi*(0:(winSize-1))/winSize;
    
    % Map to -pi/pi range
    deltaPhiPrimeMod = mod(deltaPhiPrime+pi, 2*pi) - pi;
     
    % Get the true frequency
    trueFreq = 2*pi*(0:(winSize-1))/winSize + deltaPhiPrimeMod/hop;

    % Get the final phase
    phaseCumulative = phaseCumulative + hopOut * trueFreq;    
    
    % Remove the 60 Hz noise. This is not done for now but could be
    % achieved by setting some bins to zero.
   
%% Synthesis    
    
    % Get the magnitude
    outputMag = magFrame;
    
    % Produce output frame
    outputFrame = real(ifft(outputMag .* exp(1i*phaseCumulative)));
   
    
    % Save frame that has been processed
    outputy(index,:) = outputFrame .* wn' / sqrt(((winSize/hopOut)/2));
    
    FFT_window_out = fft(outputy(index,:));
    
    dt = 1/Fs; % okres probkowania
    df = 1/(length(FFT_window_out)*dt); %czestotliwosc podstawowa f- = df = 1/T
    f_axis = df * (0:length(FFT_window_out)-1); % kolejne czestotliwosci
    significantFFT = abs(FFT_window_out);

    [max_freq, max_spectrum, max_spectrum_ID] = interpolation(significantFFT, Fs, 1);
    realDominantFreqs(index) = max_freq;

%    if index == 130
%      
%          hold on; stem(f_axis(from:MAX_COUNT), significantFFT(from:MAX_COUNT),'*r',  'LineWidth',3);
%         grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('After');
%         
%       plot(dominantFreq, 0,'o', 'LineWidth',2,...
%             'MarkerEdgeColor','k',...
%             'MarkerFaceColor','b',...
%             'MarkerSize',10);
%         
%         plot(max_freq, 0,'o', 'LineWidth',2,...
%             'MarkerEdgeColor','k',...
%             'MarkerFaceColor','r',...
%             'MarkerSize',10);
%         
%     end
        
end


%% Finalize

% Overlap add in a vector
outputTimeStretched = fusionFrames(outputy,hopOut);

% Resample with linearinterpolation
outputTime = interp1((0:(length(outputTimeStretched)-1)),outputTimeStretched,(0:alpha:(length(outputTimeStretched)-1)),'linear');

% Return the result
outputVector = outputTime;

return