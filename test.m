clear all;
close all;
[x, Fs] = wavread('kasia.wav');
	


N = length(x); % liczba probek
A = 5; % amplituda
fp = Fs; % czestotliwosc probkowania
dt = 1/Fs; % okres probkowania
t = dt*(0:N-1); % wektor czasu
f1 = 100;
f2 = 200;
f3 =300;
%x = 1*sin(2*pi*f1*t) + 2*sin(2*pi*f2*t) +  1*sin(2*pi*f3*t) +1*sin(2*pi*2*f1*t) + 1*sin(2*pi*4*f1*t) + 1*sin(2*pi*6*f1*t) + 1*sin(2*pi*8*f1*t) ;   % sinusy o roznych czestotliwosciach
x = 1*sin(2*pi*100*t) + 1.5*sin(2*pi*200*t) +  1*sin(2*pi*300*t) +1*sin(2*pi*400*t) + 1*sin(2*pi*500*t) + 1*sin(2*pi*600*t) + 1*sin(2*pi*700*t) ; 
df = 1/(N*dt); %czestotliwosc podstawowa f- = df = 1/T
f_axis = df * (0:N-1); % kolejne czestotliwosci

% Obetnijmy sobie wykresy wynikowe, zeby bylo lepiej widac skale
MAX_COUNT = N/8;

%[x, Fs] = wavread('kasia.wav');

x_fft = abs(fft(x));
figure; stem(f_axis(1:MAX_COUNT), x_fft(1:MAX_COUNT)); 
grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('Sygnal poczatkowy');

% [z,p,k] = cheby2(6,20, [0.01/(Fs/2), 2.2*Gama(end)/(Fs/2)], 'bandpass');
% SOS = zp2sos(z,p,k);
% xFiltered = sosfilt(SOS,x); 
% x_fil_fft = abs(fft(xFiltered));
% figure; stem(f_axis(1:MAX_COUNT), x_fil_fft(1:MAX_COUNT)); 
% grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('Po filtracji');

[y, newF, oldF] = pitchShift(x, 1024 ,256, Fs);
y_fft = abs(fft(y));
figure;stem(f_axis(1:MAX_COUNT), y_fft(1:MAX_COUNT)); 
grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('Po autotune');

disp('Dominujace czestotliwosci - powinny byc zblizone do tych podanych jako Gama')
for i = 1: MAX_COUNT
    if y_fft(i) > max(y_fft)/2
        f_axis(i)
    end
end

% [y2, newF2, oldF2] = pitchShift(y, 1024 ,256, Fs);
% y_fft2 = abs(fft(y2));
% figure;stem(f_axis(1:MAX_COUNT), y_fft2(1:MAX_COUNT)); 
% grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('Po autotune2');

z = pitchShiftOrig(y,1024,256,-0.5);


wavwrite(y, Fs, 'kasiaout-nd2023-zfiltrem.wav');
%wavwrite(y2, Fs, 'kasiaout2zero.wav');
wavwrite(z, Fs, 'kasiaoutpitched01.wav');