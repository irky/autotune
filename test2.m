clear all; close all;
[x, Fs] = wavread('adele_in.wav');

x = shannon_filter(x);

[y01, y01a, y01b] = pitchShiftTest2(x, 1024 ,256, Fs, 0,1);
[y02, y02a, y02b] = pitchShiftTest2(x, 1024 ,256, Fs, 0,2);

[y11, y11a, y11b] = pitchShiftTest2(x, 1024 ,256, Fs, 1, 1);
[y12, y12a, y12b] = pitchShiftTest2(x, 1024 ,256, Fs, 1,2);


y01a = y01a';
y01b = y01b';

blad_0_0 = abs(y01a - y01b);

y02a = y02a';
y02b = y02b';

blad_0_2 = abs(y02a - y02b);

y12a = y12a';
y12b = y12b';

blad_i_0 = abs(y12a - y12b);

y11a = y11a';
y11b = y11b';

blad_i_2 = abs(y11a - y11b);



wavwrite(y01, Fs, 'y01.wav');
wavwrite(y02, Fs, 'y02.wav');
wavwrite(y11, Fs, 'y11.wav');
wavwrite(y12, Fs, 'y12.wav');