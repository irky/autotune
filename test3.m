clear all; close all;
[x, Fs] = wavread('seether.wav');

x = shannon_filter(x);
N = length(x); % liczba probek
fp = Fs; % czestotliwosc probkowania
dt = 1/Fs; % okres probkowania
t = dt*(0:N-1); % wektor czasu
df = 1/(N*dt); %czestotliwosc podstawowa f- = df = 1/T
f_axis = df * (0:N-1); % kolejne czestotliwosci
scale_divider = 4;
MAX_COUNT = N/scale_divider;

ynat_fft = abs(fft(x));
figure; stem(f_axis(1:MAX_COUNT), ynat_fft(1:MAX_COUNT),'x',  'LineWidth',2);
grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('natural');


[y11n1024, y01a, y01b] = pitchShiftTest2(x, 1024 ,256, Fs, 1,1);
[y11n256, y02a, y02b] = pitchShiftTest2(x, 256 ,64, Fs, 1,1);
[y12n1024, y11a, y11b] = pitchShiftTest2(x, 1024 ,256, Fs, 1, 2);
[y12n256, y12a, y12b] = pitchShiftTest2(x, 256 ,64, Fs, 1,2);
[y12n512HOP, y12a, y12b] = pitchShiftTest2(x, 2048 ,512, Fs, 1,2);

wavwrite(y11n1024, Fs, 'y11n1024.wav');
wavwrite(y11n256, Fs, 'y11n256.wav');
wavwrite(y12n1024, Fs, 'y12n1024.wav');
wavwrite(y12n256, Fs, 'y12n256.wav');
wavwrite(y12n512HOP, Fs, 'y12n512HOP.wav');


N = length(x); % liczba probek
fp = Fs; % czestotliwosc probkowania
dt = 1/Fs; % okres probkowania
t = dt*(0:N-1); % wektor czasu
df = 1/(N*dt); %czestotliwosc podstawowa f- = df = 1/T
f_axis = df * (0:N-1); % kolejne czestotliwosci


y11n1024_fft = abs(fft(y11n1024));
figure; stem(f_axis(1:MAX_COUNT), y11n1024_fft(1:MAX_COUNT),'x',  'LineWidth',2);
grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('y11n1024');

y11n256_fft = abs(fft(y11n256));
figure; stem(f_axis(1:MAX_COUNT), y11n256_fft(1:MAX_COUNT),'x',  'LineWidth',2);
grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('y11n256');

y12n1024_fft = abs(fft(y12n1024));
figure; stem(f_axis(1:MAX_COUNT), y12n1024_fft(1:MAX_COUNT),'x',  'LineWidth',2);
grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('y12n1024');

y12n256_fft = abs(fft(y12n256));
figure; stem(f_axis(1:MAX_COUNT), y12n256_fft(1:MAX_COUNT),'x',  'LineWidth',2);
grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('y12n256');


y12n512HOP_fft = abs(fft(y12n512HOP));
figure; stem(f_axis(1:MAX_COUNT), y12n512HOP_fft(1:MAX_COUNT),'x',  'LineWidth',2);
grid; grid minor; title('Abs(x)'); xlabel('Hz'); title('y12n512HOP');