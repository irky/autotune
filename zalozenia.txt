1. Maksymalne opoznienie dla urzadzenia auto-tune to 50 ms. Powyzej tego progu bedzie zauwazalne dla sluchacza.
2. Toolbox matlabowy, z ktorego bedziemy korzystac przy testowaniu algorytmu w czasie rzeczywistym: Acquisition Toolbox. Wykorzystamy do tego karte dzwiekowa.
3. Bardzo wazne jest, aby ramki nie czekaly w kolejce - musza byc przetwarzane jedna za druga.

========= Algorytm Pitch Shifting =========
1. Jesli chodzi o jump(hop), to w fazie akwizycji i syntesy ma on rozna wartosc (tak samo delta_t, ktora od nich zalezy).
2. Odnosnie implementacji - wartosc step musimy sobie za każdym razem obliczyć, bo to jest ilosc prazkow, o ktora sie przesuwamy. Przy czym w naszym przypadku to bedzie troche inna bajka, bo ta wartosc może być ulamkowa, jako ze po prostu nie wstrzelamy się w czysty polton. Wydaje mi sie, ze w naszym przypadku step to wlasnie delta.
